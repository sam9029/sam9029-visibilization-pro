# 说明Doc

1：基于提供的前端静态资源和后端接口清单，复现前端页面

2：所用技术栈为vue+mapbox+echarts+element-ui

3：注意地图和表格的美观度

4：最后需提交代码和结果（网页地址或页面截图均可），重要代码处作注释

5：中心点的话可以前端用函数直接生成

> [Vue中mapbox的使用_steamedbread321的博客-CSDN博客_vue使用mapbox](https://blog.csdn.net/weixin_44766633/article/details/109515280#:~:text=Vue中mapbox的使用 1： 首先下载包文件 cnpm i mapbox-gl -S 1,导入包文件（main.js中导入） import mapBoxGl from 'mapbox-gl' Vue.prototype.%24mapboxgl %3D mapBoxGl)

---

# 页面效果

![img](.\图片2.png)

~~~js
// 顶部 人口结构 button 背景
#956D1C

// 热力图 颜色
#78FA8A
#ADFD52
#FCE148
#FAAA42
#FF712F
#FA3131
#B80E0E

["#B80E0E","#FA3131","#FF712F","#FAAA42","#FCE148","#ADFD52","#78FA8A"]

// 底部人像橙色
#FF9500
// 底部按钮蓝色背景
#0B2C57

// 白色字体
#fffff
// 蓝色字体
#00B2E8
//浅绿字体
#02F2F3

// 橙色点
#FFC000
~~~

---

# 接口Doc

> [API-可视化-API 管理-space-vjhdk-Eolink APIKit](https://space-vjhdk.w.eolink.com/home/api_studio/inside/api/list?groupID=-1&projectHashKey=5rkAA5nfe1ee5cf64964046c76492249e9ccbf87239a7aa&spaceKey=space-vjhdk)

## 一：页面的接口参数：

（1）month    月份,传参为“03”    query    true    string
（2）value    区县, 传参为“510112”    query    true    string
（3）year    年份，传参为“2022”,    query    true    string

## 对应接口

#### get:

（1）常住人口总量：http://171.212.102.6:8399/api/v1/pop-struct/res/amount/?value=510112&month=03&year=2022

（2）常住人口平均年龄和男女占比：http://171.212.102.6:8399/api/v1/pop-struct/res/huaxiang/?value=510112002

（3）常住人口年龄段：http://171.212.102.6:8399/api/v1/pop-struct/res/rknianljzt?value=510112002

（4）常住人口街道排行: http://171.212.102.6:8399/api/v1/pop-struct/res/jiedaoOD;

#### post

(5)登录接口:http://171.212.102.6:8399/api/v1/user/login

~~~js
{ "loginName": "test", "password": "123456"}
// 账号:test
// 密码:123456
~~~



----

# 参考文章：

### 教程

> [【Vue+Mapbox】Vue中mapbox地图的使用（一）_DoYouKnowArcgis的博客-CSDN博客_vue mapbox](https://blog.csdn.net/yuelizhe4774/article/details/125975864)
>
> [【Vue+Mapbox】Vue中mapbox地图的使用（二）—— 自定义Geojson地图数据_DoYouKnowArcgis的博客-CSDN博客_mapbox 加载geojson](https://blog.csdn.net/yuelizhe4774/article/details/125977994?ops_request_misc=&request_id=&biz_id=102&utm_term=mapbox geojson&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-125977994.nonecase&spm=1018.2226.3001.4187)
>
> [【Vue+Mapbox】Vue中mapbox地图的使用（三）——mapbox对象在组件中共享_DoYouKnowArcgis的博客-CSDN博客](https://blog.csdn.net/yuelizhe4774/article/details/126021553)
>
> (不太相关)[mapbox使用教程_javascript_跳跳的小古风-DevPress官方社区 (csdn.net)](https://huaweicloud.csdn.net/638ee031dacf622b8df8d4e8.html?spm=1001.2101.3001.6650.3&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-3-124520280-blog-126758209.pc_relevant_3mothn_strategy_recovery&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-3-124520280-blog-126758209.pc_relevant_3mothn_strategy_recovery&utm_relevant_index=6)
>
> 

### 官网

> - [Maps, geocoding, and navigation APIs & SDKs | Mapbox](https://www.mapbox.com/)
>   - token
>
> ~~~JS
> pk.eyJ1Ijoic3E4ODg1NTUiLCJhIjoiY2toNDdudTN6MDByNjMzbW84cDA2MWJxMSJ9.o191y8mStuwyy-URFaq5Uw
> ~~~
>
> - 资源：感谢您的回答！中文版《Mapbox 地图设计指南》下载地址如下：
>   链接: https://pan.baidu.com/s/1Wwc__uxJG0EfbIzl1ET3_Q 提取码: air6
>
> - **❓注册需要卡号的问题**：
>   - [mapbox的注册怎么需要填卡号，有银行要求吗，之前看网上说不需要填，具体要怎么操作，求大神指点。？ - 知乎 (zhihu.com)](https://www.zhihu.com/question/528431466)
>   - [信用卡生成器 - 随机大全 (suijidaquan.com)](https://www.suijidaquan.com/credit-card-generator)

## 其他问题

#### - vue项目运行出现报错：

Multiple assets emit different content to the same filename index.html

- > [ vue项目运行出现报错：Multiple assets emit different content to the same filename index.html_橘子972的博客-CSDN博客_multiple assets emit different content to the same](https://blog.csdn.net/weixin_63722092/article/details/124592126?spm=1001.2101.3001.6650.10&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-10-124592126-blog-127570507.pc_relevant_3mothn_strategy_and_data_recovery&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-10-124592126-blog-127570507.pc_relevant_3mothn_strategy_and_data_recovery&utm_relevant_index=12)

  - 降级 vue/cli 	
  - 先卸载新版本
    npm uninstall @vue/cli
    然后安装旧版本
    npm install @vue/cli@4.5.17

#### - ❗vue 引入geojson格式文件 需要 npm i json-loader

- [ vue 引入geojson格式文件_我请你吃炫迈的博客-CSDN博客_vue 引入geojson](https://blog.csdn.net/qq_38848323/article/details/120764617)

- 并配置 vue.config.js

- ~~~js
  // 
  module.exports = {
      // vue 的 config 一定要写configureWebpack 之内
      // webpack 就直接配
  	configureWebpack: {
          module: {
              rules: [
                  {
                      test: /\.geojson$/,
                      loader: 'json-loader'
                  }
              ]
          },
      }
  }
  ~~~

- 

- 否则报错：

~~~js
Module parse failed: Unexpected token (2:6)
You may need an appropriate loader to handle this file type, currently no loaders are configured to process this file. See https:*//webpack.js.org/concepts#loaders*
| {
\> "type": "FeatureCollection",
| "name": "成都_街镇_2020_GCJ02_龙泉驿区",
| "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
~~~



# geojson文件

> [GeoJSON详解（带图）_PzLu的博客-CSDN博客_geojson](https://blog.csdn.net/Richard__Ting/article/details/99319860)
>
> 