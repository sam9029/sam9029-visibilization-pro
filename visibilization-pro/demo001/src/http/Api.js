import request from './request.js'

// 登录 =================================================
/**
 * @param {String} loginName 
 * @param {String} password
 * 
 * { "loginName": "test", "password": "12456"}
 *
 * */
export function loginApi(_data){
	return request({
		url:'/user/login',
        method:"post",
        data:_data
	})
}

/**
 * 公用
 *month    月份,传参为“03”    query    true    string
 *value    区县,传参为“510112”    query    true    string
 *year     年份,传参为“2022”,    query    true    string
 * 
 * */ 
let defaultParam1 = {mouth:"03",value:'510112',year:'2022'}
let defaultParam2 = {mouth:"03",value:'510112002',year:'2022'}

// 常住人口总量 =================================================
export function getPopulationApi(_data){
	return request({
		url:'/pop-struct/res/amount/',
        method:"get",
        data: (_data || defaultParam1)
	})
}


// 常住人口平均年龄和男女占比 =================================================
export function getAverageAgeAndSexRatioApi(_data){
	return request({
		url:'/pop-struct/res/huaxiang/',
        method:"get",
        data: (_data || defaultParam2)
	})
}

// 常住人口年龄段 =================================================
export function getAgeGroupApi(_data){
	return request({
		url:'/pop-struct/res/rknianljzt',
        method:"get",
        data: (_data || defaultParam1)
	})
}

// 常住人口街道排行 =================================================
export function getStreetsPopulationRankApi(_data){
	return request({
		url:'/pop-struct/res/jiedaoOD',
        method:"get",
        data: (_data || defaultParam2)
	})
}