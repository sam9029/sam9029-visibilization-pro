// 储存
function handleSetStorage(_key, _val) {
  _key = "s9_" + _key;
  localStorage.setItem(_key, _val);
}

// 取出
function handleGetStorage(_key) {
  _key = "s9_" + _key;
  return localStorage.getItem(_key);
}

// 移除
function handleRemoveStorage(_key) {
  _key = "s9_" + _key;
  localStorage.removeItem(_key);
}

export { handleSetStorage, handleGetStorage, handleRemoveStorage };
