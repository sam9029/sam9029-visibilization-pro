import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import mapBoxGl from 'mapbox-gl';
import { Button, DatePicker } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 挂载前 引入自适应 JS
import './utils/responseRem.js'
import './assets/css/base.css';
import './assets/css/common.scss';

import {loginApi} from './http/Api.js'
import {handleSetStorage,handleGetStorage} from './http/auth.js'

// 登录
let flag = false;
if(!handleGetStorage('token')){
  loginApi({
      "loginName": "test",
      "password": "123456"
  }).then(res=>{
      console.log(res);
      if(res.data.token){
          handleSetStorage('token',res.data.token)
      }
  }).catch(err=>console.log(err))
}


Vue.prototype.$mapboxgl = mapBoxGl;
Vue.config.productionTip = false;

Vue.use(Button)
Vue.use(DatePicker)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
